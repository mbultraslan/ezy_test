<?php
// allow origin for ajax CORS
header("Access-Control-Allow-Origin: *");
require_once __DIR__ . '/vendor/autoload.php';

if (isset($_GET['action'])) {
    switch ($_GET['action']) {
        case 'get_products' :
            $product = new \EzyVet\Controller\ProductController();
            $product->getProducts();
            break;
        case 'checkout':
            // do checkout
            break;
        default:
            //Exception here
            die('invalid action');
    }

} else {
    //Exception here
    die('invalid action');
};