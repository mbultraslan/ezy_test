<?php


namespace EzyVet\Model;

class ProductModel
{


    /**
     * Return products database
     *
     * @author Mehmet
     * @return array
     * @since 0.0.1
     */
    public static function  getProducts():array
    {
        // ######## please do not alter the following code ########
        $products = [
            [ "name" => "Sledgehammer", "price" => 125.75 ],
            [ "name" => "Axe", "price" => 190.5 ],
            [ "name" => "Bandsaw", "price" => 562.131 ],
            [ "name" => "Chisel", "price" => 12.9 ],
            [ "name" => "Hacksaw", "price" => 18.45 ],
        ];
        // ########################################################
        return $products;
    }

}