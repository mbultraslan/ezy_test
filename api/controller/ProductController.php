<?php


namespace EzyVet\Controller;

use EzyVet\Model\ProductModel as productModel;
use EzyVet\Libs\System as System;

/**
 * Class ProductController
 *
 * @author Mehmet
 * @package EzyVet\Controller
 * @since 0.0.1
 */
class ProductController
{
	/**
	 * Get products and response call
	 *
	 * @author mehmet
	 * @since 0.0.1
	 */
    public function getProducts()
    {
        $products = productModel::getProducts();
        System::response($products);

    }

}