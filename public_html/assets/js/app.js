var app = {

    apiEndPoint: 'http://localhost/projects/cart/api/',

    init: function () {
        app.loadProducts();
        app.renderCart();
    },


    /**
     * Get Products fro API
     *
     * @author Mehmet
     * @since 0.0.1
     */
    loadProducts: function () {
        $.get(app.apiEndPoint + '?action=get_products', function (data, statusText, xhr) {
            if (xhr.status !== 200) {
                alert('Error... Please Make sure you set api end point');
                console.log(statusText);
            } else if (!data.length) {
                alert('There is no product :(')
            } else {
                app.renderProducts(data);
            }
        })
    },


    /**
     * Render Products
     *
     * @author Mehmet
     * @since 0.0.1
     */
    renderProducts: function (products) {
        $('#products').html('');
        $.each(products, function (index, productData) {
            var product = app.renderProduct(productData);
            $('#products').append(product);
        });
    },

    /**
     * return single product item
     *
     * @author Mehmet
     * @since 0.0.1
     */
    renderProduct: function (productData) {
        var html = '';
        html += '<div class="col-md-3" style="margin-top:12px;">';
        html += '    <div style="border:1px solid #333; background-color:#f1f1f1; border-radius:5px; padding:16px; height:350px;" align="center">';
        html += '        <img src="https://via.placeholder.com/150" class="img-responsive img-thumbnail" /><br />';
        html += '        <h4 class="text-info">' + productData.name + '</h4>';
        html += '        <h4 class="text-danger">' + app.formatMoney(productData.price) + '</h4>';
        html += '        <input type="button" onclick="app.addToCart(\'' + productData.name + '\',\'' + productData.price + '\')" name="add_to_cart"  style="margin-top:5px;" class="btn btn-success form-control add_to_cart" value="Add to Cart" />';
        html += '    </div>';
        html += '</div>';
        return html;
    },

    /**
     * Render cart dropdown
     *
     * @author Mehmet
     * @since 0.0.1
     */
    renderCart: function () {
        var total = 0;
        var html = '';
        var productsInCart = app.getCart();
        if (productsInCart.length) {
            html += '<div class="table-responsive" id="order_table">';
            html += '    <table class="table table-bordered table-striped">';
            html += '        <tr>';
            html += '            <th>Product Name</th>';
            html += '            <th>Quantity</th>';
            html += '            <th>Price</th>';
            html += '            <th>Total</th>';
            html += '            <th>Action</th>';
            html += '        </tr>';

            $.each(productsInCart, function (index, product) {
                var productTotal = product.qty * parseFloat(product.price);
                total += productTotal;
                html += '<tr>';
                html += '    <td>' + product.name + '</td>';
                html += '    <td>' + product.qty + '</td>';
                html += '    <td>' + app.formatMoney(product.price) + '</td>';
                html += '    <td>' + app.formatMoney(productTotal) + '</td>';
                html += '    <td class="text-nowrap">';
                html += '       <span class="btn btn-danger btn-xs" onclick="app.removeItem(\'' + product.name + '\')">X</span> ';
                if (product.qty > 1) {
                    html += '   <span class="btn btn-warning btn-xs" onclick="app.decreaseProductInCart(\'' + product.name + '\')">-</span> ';
                }
                html += '       <span class="btn btn-success btn-xs" onclick="app.increaseProductInCart(\'' + product.name + '\')">+</span>';
                html += '   </td>';
                html += '</tr>';
            });

            html += '    <tr>';
            html += '       <td colspan="3" class="text-right">Total</td>';
            html += '       <td  class="text-right">' + app.formatMoney(total) + '</td>';
            html += '       <td></td>';
            html += '    </tr>';
            html += '  </table>';
            html += '  </div>';
            html += '<div align="right">';
            html += '   <a href="#" class="btn btn-default" id="clear_cart" onclick="app.clearCart()">';
            html += '       <span class="glyphicon glyphicon-trash"></span>Clear';
            html += '   </a>';
            html += '</div>';
        } else {
            html += 'Your cart is empty';
        }
        $('#cart_details').html(html);
        $('#total_price').html(app.formatMoney(total));

    },

    /**
     * Format money
     *
     * @author Mehmet
     * @since 0.0.1
     */
    formatMoney: function (amount) {
        return new Intl.NumberFormat('en-GB', {
            style: 'currency',
            currency: 'GBP'
        }).format(amount);
    },

    /**
     * Add to cart
     *
     * @author Mehmet
     * @since 0.0.1
     */
    addToCart: function (name, price) {
        var productsInCart = app.getCart();
        if (productsInCart.length && app.findProductInCart(name) !== false) {
            app.increaseProductInCart(name);
            alert('One more item added');
        } else {
            product = {
                name: name,
                price: price,
                qty: 1
            };
            productsInCart.push(product);
            app.setCart(productsInCart);
            alert('new item added')
        }
        app.renderCart();

    },

    /**
     * Get cart products from local storage
     * If we had logged in player then it is always better to store in DB
     *
     * @author Mehmet
     * @since 0.0.1
     */
    getCart: function () {
        if (localStorage.getItem('ezyCart')) {
            return JSON.parse(localStorage.getItem('ezyCart'));
        }
        return [];
    },

    /**
     * Set cart data to local storage
     *  If we had logged in player then it is always better to store in DB
     *
     * @author Mehmet
     * @since 0.0.1
     */
    setCart: function (data) {
        console.log(data);
        localStorage.setItem('ezyCart', JSON.stringify(data));
    },


    /**
     * Clear cart data from local storage
     *
     * @author Mehmet
     * @since 0.0.1
     */
    clearCart() {
        localStorage.removeItem('ezyCart');
        app.renderCart();
    },

    /**
     * Find product by name from local storage
     *
     * @author Mehmet
     * @since 0.0.1
     */
    findProductInCart: function (name) {
        var result = false;
        var cart = app.getCart();
        $.each(cart, function (index, product) {
            if (product.name === name) {
                result = product;
                return false;
            }
        });
        return result;
    },

    /**
     * increase product qty
     *
     * @author Mehmet
     * @since 0.0.1
     */
    increaseProductInCart: function (name) {
        var cart = app.getCart();
        $.each(cart, function (index, product) {
            if (product.name === name) {
                cart[index]['qty']++;
                app.setCart(cart);
            }
        });
        app.renderCart();
    },

    /**
     * Decrease product qty
     *
     * @author Mehmet
     * @since 0.0.1
     */
    decreaseProductInCart: function (name) {
        var cart = app.getCart();
        $.each(cart, function (index, product) {
            if (product.name === name) {
                --cart[index]['qty'];
                app.setCart(cart);
            }
        });
        app.renderCart();
    },

    /**
     * remove product from local storage
     *
     * @author Mehmet
     * @since 0.0.1
     */
    removeItem: function (name) {
        var cart = app.getCart();
        $.each(cart, function (index, product) {
            if (product.name === name) {
                cart.splice(index, 1);
                app.setCart(cart);
            }
        });
        app.renderCart();
    }

}